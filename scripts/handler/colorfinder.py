list_color = []


def displaydata(jsondata):
    if not isinstance(jsondata, str):
        for k, v in jsondata.items():
            if isinstance(v, dict):
                displaydata(v)
            elif hasattr(v, '__iter__') and not isinstance(v, str):
                for items in v:
                    displaydata(items)
            elif isinstance(v, str):
                if k == "color":
                    list_color.append(v)
            else:
                if k == "color":
                    list_color.append(v)
    return set(list_color)
