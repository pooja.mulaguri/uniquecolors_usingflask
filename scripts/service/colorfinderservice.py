from flask import request, Blueprint
from scripts.handler.colorfinder import displaydata

blueprint=Blueprint("blueprint", __name__)
@blueprint.route('/', methods = ['POST'])
def postJsonHandler():
    content = request.get_json()
    requesteddata=displaydata(content)
    return str(requesteddata)