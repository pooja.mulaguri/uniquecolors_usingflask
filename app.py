from flask import Flask
from scripts.service.colorfinderservice import blueprint
app=Flask('__name__')
app.register_blueprint(blueprint)

if __name__ == '__main__':
    app.run()
# Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
